package com.example.wsrkazan

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer

class OnBoardingActivity : AppCompatActivity() {

    private lateinit var sharedPreferences: SharedPreferences
    private var tokenExists: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)
        sharedPreferences = getSharedPreferences("main",Context.MODE_PRIVATE)
        val token: String? = sharedPreferences.getString("token", null)

        if (token!!.isNotEmpty()) {
            tokenExists = true
        }

        val timer = object : CountDownTimer(2000, 200) {
            override fun onTick(p0: Long) {
            }
            override fun onFinish() {
                if (!tokenExists) {
                    val intent = Intent(this@OnBoardingActivity, TutorialActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    val intent = Intent(this@OnBoardingActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
        timer.start()
    }
}