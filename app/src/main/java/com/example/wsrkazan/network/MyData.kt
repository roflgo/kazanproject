package com.example.wsrkazan.network

import android.widget.ImageView
import android.widget.TextView

data class TutorialClass(val image: Int, val textMain: String, val textSecond: String)
